class Potepan::ProductsController < ApplicationController
  THE_NUMBER_OF_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Variant.find(params[:id])
    @product_properties = @product.product.product_properties.includes(:property)
    @related_products = Spree::Product.includes_prices_and_images.
      related_products(@product.product).
      exclude_product(@product.product).
      limit(THE_NUMBER_OF_RELATED_PRODUCTS)
  end
end
