class Potepan::HomeController < ApplicationController
  THE_NUMBER_OF_PRODUCTS = 9
  def index
    @new_products = Spree::Product.
      includes_prices_and_images.
      order(available_on: :desc).
      limit(THE_NUMBER_OF_PRODUCTS)
  end
end
