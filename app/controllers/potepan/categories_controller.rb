class Potepan::CategoriesController < ApplicationController
  before_action :common

  def common
    @taxonomies = Spree::Taxonomy.all.includes(:root)
    @color_list = Spree::OptionType.find_by(name: 'tshirt-color').option_values
    @size_list = Spree::OptionType.find_by(name: "tshirt-size").option_values
    @display = params[:display] == "list" ? "list" : "grid"
  end

  def show
    @taxon = Spree::Taxon.find(params[:id])
    # @taxonomies = Spree::Taxonomy.all.includes(:root)
    @products = Spree::Product.in_taxon(@taxon).
      includes_prices_and_images.
      reorder(nil).
      order_by(params[:order])
    # @display = params[:type] == "list" ? "list" : "grid"
  end

  def index
    @option_value = Spree::OptionValue.find_by(name: params[:option_name])
    if params[:option_name].present?
      @products = @option_value.variants.
        includes([:product, :default_price]).
        reorder(nil).
        order_by(params[:order])
    else
      @products = Spree::Product.all
    end
  end
end
