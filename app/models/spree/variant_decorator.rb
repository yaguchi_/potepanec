Spree::Variant.class_eval do
  scope :sort_descend_by_price,               -> { order("spree_prices.amount DESC") }
  scope :sort_ascend_by_price,                -> { order("spree_prices.amount ASC") }
  scope :sort_newest_by_product_available_on, -> { order("spree_products.available_on DESC") }
  scope :sort_oldest_by_product_available_on, -> { order("spree_products.available_on ASC") }
  scope :order_by,                            -> (order) {
                                                case order
                                                when '高い順' then sort_descend_by_price
                                                when '安い順' then sort_ascend_by_price
                                                when '古い順'
                                                  then sort_oldest_by_product_available_on
                                                else sort_newest_by_product_available_on
                                                end
                                              }
end
