require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    let(:product1) { create(:product, available_on: 1.month.ago) }
    let(:product2) { create(:product, available_on: 2.month.ago) }
    let(:product3) { create(:product, available_on: 3.month.ago) }
    let(:product4) { create(:product, available_on: 4.month.ago) }
    let(:new_products) { [ product1, product2, product3, product4 ] }
    # let(:new_products2) { [ product4, product3, product2, product1 ] }
    subject(:products) { assigns(:new_products) }

    before { get :index }

    it "renders :show successfully" do
      expect(response).to render_template :index
    end

    it "returns HTTP status 200 response" do
      expect(response).to have_http_status 200
    end

    it "assigns @new_products" do
      expect(products).to eq new_products
    end

    it 'assigns @new_products order correctly' do
      expect(products).to match new_products
    end

    context "when give 2 products" do
      let!(:new_products_2) { [ product1, product2 ] }

      it "has 4 products" do
        expect(products.count).to eq 2
      end
    end
  end
end
