require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:product) { create(:product) }
    let(:product_property) { create(:product_property, product: product, property: property) }
    let(:property) { create(:property) }
    before { get :show, params: { id: product.id } }

    it "renders :show successfully" do
      expect(response).to render_template :show
    end

    it "returns HTTP status 200 response" do
      expect(response).to have_http_status 200
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'assigns @product_properties' do
      expect(assigns(:product_properties)).to match_array product_property
    end
  end

  describe "@related_products" do
    let(:product) { create(:product, taxons: [ taxon ]) }
    let(:taxon) { create(:taxon) }
    let(:related_products) { create_list(:product, 4, taxons: [ taxon ]) }
    before { get :show, params: { id: product.id } }

    it "assigns @related_products" do
      expect(assigns(:related_products)).to match_array(related_products)
    end

    it "doesn't include self" do
      expect(assigns(:related_products)).not_to include product
    end

    describe "@related_products has some products" do
      subject { assigns(:related_products).count }

      context "when give 3 products" do
        before { create_list(:product, 3, taxons: [ taxon ]) }

        it "has 3 products" do
          is_expected.to eq 3
        end
      end

      context "when give 4 products" do
        before { create_list(:product, 4, taxons: [ taxon ]) }

        it "has 4 products" do
          is_expected.to eq 4
        end
      end

      context "when give 5 products" do
        before { create_list(:product, 5, taxons: [ taxon ]) }

        it "has only 4 products" do
          is_expected.to eq 4
        end
      end
    end
  end
end
