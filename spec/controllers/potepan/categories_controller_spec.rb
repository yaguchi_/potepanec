require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:taxon_child) { create(:taxon, parent: taxon, taxonomy: taxonomy, products: [ product_child ]) }
    let(:product_child) { create(:product, price: 19.98) }
    before { get :show, params: { id: taxon_child.id } }

    it "renders :show successfully" do
      expect(response).to render_template :show
    end
    it "returns HTTP status 200 response" do
      expect(response).to have_http_status "200"
    end
    it 'assigns @taxon' do
      expect(assigns(:taxon)).to eq taxon_child
    end
    it "assigns @taxonomies" do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end
    it 'assigns @products' do
      expect(assigns(:products)).to match_array product_child
    end
  end
end
