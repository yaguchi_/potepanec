require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_title helper test' do

    context 'when give test_title' do
      it 'returns test_title' do
        expect(full_title('test_title')).to eq 'test_title - BIGBAG Store'
      end
    end

    context 'when give blank' do
      it 'returns base_title' do
        expect(full_title("")).to eq 'BIGBAG Store'
      end
    end
  end
end
