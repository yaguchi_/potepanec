require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "Scope" do
    let(:product) { create(:product, taxons: [ taxon ]) }
    let(:product2) { create(:product, taxons: [ taxon2 ]) }
    let(:taxon) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:related_product) { create(:product, taxons: [ taxon ]) }

    it "has related_products" do
      expect(Spree::Product.related_products(product)).to include related_product
      expect(Spree::Product.related_products(product2)).to_not include related_product
    end

    it "excludes current product" do
      expect(Spree::Product.exclude_product(product)).not_to include product
    end
  end
end
