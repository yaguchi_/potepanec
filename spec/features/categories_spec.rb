require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id, children: [ taxon_child ], products: [ product ]) }
  given(:taxon_child) { create(:taxon, taxonomy: taxonomy, name: "taxon_child", products: [ product_child ]) }
  given(:product) { create(:product) }
  given(:product_child) { create(:product, price: 19.98) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "display specified products in taxon page" do
    expect(page).to have_current_path(potepan_category_path(taxon.id))
    expect(page).to have_title taxon.name + " - BIGBAG Store"
    expect(page).to have_link taxonomy.root.name, href: "javascript:;"
    expect(page).to have_selector '.productBox h5', text: product.name
    expect(page).to have_selector '.productBox h5', text: product_child.name
    expect(page).to have_selector '.productBox h3', text: product.display_price
    expect(page).to have_selector '.productBox h3', text: product_child.display_price
  end

  scenario "display specified products in taxon_child page" do
    click_on taxon_child.name

    expect(page).to have_current_path(potepan_category_path(taxon_child.id))
    expect(page).to have_title taxon_child.name + " - BIGBAG Store"
    expect(page).to have_link taxonomy.root.name, href: "javascript:;"
    expect(page).to_not have_selector '.productBox h5', text: product.name
    expect(page).to have_selector '.productBox h5', text: product_child.name
    expect(page).to_not have_selector '.productBox h3', text: product.display_price
    expect(page).to have_selector '.productBox h3', text: product_child.display_price
  end

  scenario "transition to product page and display specified products" do
    click_on product.name

    expect(page).to have_current_path(potepan_product_path(product.id))
    expect(page).to have_title product.name + " - BIGBAG Store"
    expect(page).to have_selector '.media-body h2', text: product.name
    expect(page).to have_selector '.media-body h3', text: product.display_price
  end

  scenario "transition to top page" do
    click_on 'logo'

    expect(page).to have_current_path(potepan_index_path)
  end

end
