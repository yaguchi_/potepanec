require 'rails_helper'

RSpec.feature "Products", type: :feature do

  given(:product) { create(:product, taxons: [taxon]) }
  given!(:related_product) { create(:product, taxons: [taxon]) }
  given!(:related_product2) { create(:product, taxons: [taxon2], price: 19.98) }
  given!(:product_property) { create(:product_property, product: product, property: property, value: "test") }
  given(:property) { create(:property) }
  given(:taxon) { create(:taxon) }
  given(:taxon2) { create(:taxon) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "display specified products" do
    expect(page).to have_current_path(potepan_product_path(product.id))

    expect(page).to have_title product.name + " - BIGBAG Store"
    expect(page).to have_selector '.media-body h2', text: product.name
    expect(page).to have_selector '.media-body h3', text: product.display_price
    expect(page).to have_selector '.media-body p', text: product.description
    expect(page).to have_selector '.list-unstyled li', text: product_property.value
    expect(page).to have_selector '.list-unstyled li', text: property.presentation
  end

  scenario "display related_products" do
    expect(page).to have_selector '.productCaption h5', text: related_product.name
    expect(page).to have_selector '.productCaption h3', text: related_product.display_price
    expect(page).not_to have_selector '.productCaption h5', text: related_product2.name
    expect(page).not_to have_selector '.productCaption h3', text: related_product2.display_price
  end

  scenario "transition to related_product page and display specified products" do
    click_link related_product.name

    expect(page).to have_current_path(potepan_product_path(related_product.id))
    expect(page).to have_selector '.media-body h2', text: related_product.name
    expect(page).to have_selector '.media-body h3', text: related_product.display_price
  end

  scenario "transition to categories page" do
    click_link '一覧ページへ戻る'

    expect(page).to have_current_path(potepan_category_path(Spree::Taxon.root.id))
  end

  scenario "transition to top page" do
    click_on 'logo'

    expect(page).to have_current_path(potepan_index_path)
  end
end
